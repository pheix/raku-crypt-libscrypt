#!/usr/bin/env raku

use v6;

use Test;
use LibraryCheck;
use Crypt::LibScrypt:ver<0.0.7+>:auth<zef:knarkhov>;
use MIME::Base64;
use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;
use Crypt::LibGcrypt::Constants;
use Crypt::LibGcrypt::Cipher;
use OpenSSL:ver<0.2.1+>;
use OpenSSL::CryptTools;

# https://github.com/ethereum/wiki/wiki/Web3-Secret-Storage-Definition#scrypt

constant derivedkey = '0xfac192ceb5fd772906bea3e118a69e8bbb5cc24229e20d8766fd298291bba6bd';
constant mac        = '0x2103ac29920d71da29f15d75b4a16dbe95cfd7ff8faea1056c33131d846e3097';
constant plaintext  = '0x7a28b5ba57c53603b0b07b56bba752f7784bf506fa95edc395f5cf6c7514fe9d';

if library-exists('scrypt', v0) {
    my $eth = Net::Ethereum.new;
    my $keccak = Node::Ethereum::Keccak256::Native.new;

    my $iv         = '83dbcc02d8ccb40e466191a123791e0e';
    my $ciphertext = 'd172bf743a674da9cdad04534d56926ef8358534d458fffccd4e6ad2fbde479c';
    my $password   = 'testpassword';
    my $kdfparams  = {
        dklen => 32,
        n     => 262144,
        p     => 8,
        r     => 1,
        salt  => 'ab0c7876052600dd703518d6fc3fe8984592145b591fc8fb5c6d43190334ba19',
    };

    my buf8 $hashbuf;
    my buf8 $saltbuf = $eth.hex2buf($kdfparams<salt>);

    lives-ok { $hashbuf = scrypt-scrypt($password, $saltbuf, $kdfparams<n>, $kdfparams<r>, $kdfparams<p>) }, 'scrypt-scrypt';

    my $derivedkey = $hashbuf.subbuf(0, $kdfparams<dklen>);

    is $eth.buf2hex($derivedkey).lc, derivedkey, 'derived key';
    is $eth.buf2hex($keccak.keccak256(:msg($derivedkey.subbuf(*-16).push($eth.hex2buf($ciphertext))))).lc, mac, 'calculated mac';

    (my $key = $eth.buf2hex($derivedkey.subbuf(0, 16)).lc) ~~ s/^0x//;

    my $gcrypt = Crypt::LibGcrypt::Cipher.new(:algorithm(GCRY_CIPHER_AES), :key($eth.hex2buf($key)), :mode('CTR'), :ctr($eth.hex2buf($iv)));

    diag($eth.hex2buf($ciphertext).gist);
    diag($eth.hex2buf($key).gist);
    diag($eth.hex2buf($iv).gist);

    my $plainkey_gcrypt  = $gcrypt.decrypt($eth.hex2buf($ciphertext), :bin(True));
    my $plainkey_openssl = decrypt($eth.hex2buf($ciphertext), :aes128ctr, :iv($eth.hex2buf($iv)), :key($eth.hex2buf($key)));

    diag($plainkey_gcrypt.gist);
    diag($plainkey_openssl.gist);

    is $eth.buf2hex($plainkey_openssl).lc, plaintext, 'secret text OpenSSL';
    is $eth.buf2hex($plainkey_openssl).lc, plaintext, 'secret text Crypt::LibGrypt';
}
else {
    skip "No libscrypt, skipping tests";
}

done-testing;
# vim: expandtab shiftwidth=4 ft=raku
